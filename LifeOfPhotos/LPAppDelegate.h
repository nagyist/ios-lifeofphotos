//
//  LPAppDelegate.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

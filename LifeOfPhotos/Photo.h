//
//  Photo.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Place, Tag;

@interface Photo : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * subtitle;
@property (nonatomic, retain) NSString * unique;
@property (nonatomic, retain) NSDate * createDate;
@property (nonatomic, retain) NSNumber * farm;
@property (nonatomic, retain) NSString * server;
@property (nonatomic, retain) NSString * secret;
@property (nonatomic, retain) NSString * originalSecret;
@property (nonatomic, retain) NSSet *tags;
@property (nonatomic, retain) Place *place;
@end

@interface Photo (CoreDataGeneratedAccessors)

- (void)addTagsObject:(Tag *)value;
- (void)removeTagsObject:(Tag *)value;
- (void)addTags:(NSSet *)values;
- (void)removeTags:(NSSet *)values;

@end

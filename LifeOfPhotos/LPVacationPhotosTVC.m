//
//  LPVacationPhotosTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPVacationPhotosTVC.h"
#import "Photo+Flickr.h"
#import "LPVacationData.h"
#import "LPPhotoVC.h"

@interface LPVacationPhotosTVC ()

@end

@implementation LPVacationPhotosTVC
@synthesize vacationName = _vacationName;
@synthesize placeName = _placeName;
@synthesize tag = _tag;

- (void)setupFetchedResultsController
{
    if (!self.vacationName || (!self.placeName && !self.tag)) return;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Photo"];
    request.sortDescriptors = @[
    [NSSortDescriptor
     sortDescriptorWithKey:@"createDate"
     ascending:YES]
    ];
    if (self.placeName) {
        request.predicate = [NSPredicate
                       predicateWithFormat:@"place.name = %@", self.placeName];
    } else if (self.tag) {
        request.predicate = [NSPredicate
                      predicateWithFormat:@"ANY tags.name = %@", self.tag.name];
    }
    
    [LPVacationData
     openVacation:self.vacationName
     usingBlock:^(UIManagedDocument *vacation){
         self.fetchedResultsController = [[NSFetchedResultsController alloc]
                                          initWithFetchRequest:request
                                          managedObjectContext:vacation.managedObjectContext
                                          sectionNameKeyPath:nil
                                          cacheName:nil];
     }];
}

- (void)setVacationName:(NSString *)vacationName
{
    if (vacationName != _vacationName) {
        _vacationName = vacationName;
        [self setupFetchedResultsController];
    }
}

- (void)setPlaceName:(NSString *)placeName
{
    if (placeName != _placeName) {
        _placeName = placeName;
        [self setupFetchedResultsController];
    }
}

- (void)setTag:(Tag *)tag
{
    if (tag != _tag) {
        _tag = tag;
        [self setupFetchedResultsController];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Vacation Photo Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Photo *photo = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = photo.title;
    cell.detailTextLabel.text = photo.subtitle;
    
    return cell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    if (self.placeName) self.navigationItem.title = self.placeName;
    if (self.tag) self.navigationItem.title = self.tag.name;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Photo"]) {
        LPPhotoVC *photoVc = segue.destinationViewController;
        Photo *photo = [self.fetchedResultsController
                        objectAtIndexPath:[self.tableView indexPathForCell:sender]];
        photoVc.photoInfo = [photo flickrPhotoInfo];
    }
}

@end

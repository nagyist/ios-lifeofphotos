//
//  LPPhotoVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPPhotoVC.h"
#import "LPCache.h"
#import "FlickrFetcher.h"
#import "LPVacationData.h"
#import "Photo+Flickr.h"

@interface LPPhotoVC () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *downloadIndicator;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbar;

@end

@implementation LPPhotoVC
@synthesize imageView = _imageView;
@synthesize scrollView = _scrollView;
@synthesize downloadIndicator = _downloadIndicator;
@synthesize toolbar = _toolbar;
@synthesize splitViewBarButtonItem = _splitViewBarButtonItem;
@synthesize photoInfo = _photoInfo;

#define MAX_RECENT_PHOTOS 20

#define PHOTO_CACHE_FACILITY @"photos"
#define PHOTO_CACHE_MAX_SIZE 10 * 1024 * 1024

- (IBAction)visitUnvisit:(UIBarButtonItem *)sender
{
    sender.enabled = NO;
    [LPVacationData
     openVacation:@"My Vacation"
     usingBlock:^(UIManagedDocument *vacation) {
         BOOL photoExist = [Photo exist:self.photoInfo
                 inManagedObjectContext:vacation.managedObjectContext];
         if (photoExist) {
             [self.navigationController popViewControllerAnimated:YES];
             [Photo remove:self.photoInfo
  fromManagedObjectContext:vacation.managedObjectContext];
             sender.title = @"Visit";
         } else {
            [Photo create:self.photoInfo
   inManagedObjectContext:vacation.managedObjectContext];
            sender.title = @"Unvisit";
         }
         sender.enabled = YES;
    }];
}

- (void)setRecentImage
{
    if (!self.photoInfo) return;
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    NSArray *recentPhotos = [defaults objectForKey:RECENT_PHOTOS_KEY];
    if (!recentPhotos) {
        recentPhotos = [NSArray arrayWithObject:self.photoInfo];
    } else {
        NSMutableArray *photos = [recentPhotos mutableCopy];
        NSString *photoId = [self.photoInfo objectForKey:FLICKR_PHOTO_ID];
        NSUInteger i = 0;
        for (NSDictionary *photoInfo in recentPhotos) {
            if ([[photoInfo objectForKey:FLICKR_PHOTO_ID] isEqualToString:photoId]) {
                [photos removeObjectAtIndex:i];
                break;
            }
            ++i;
        }
        [photos insertObject:self.photoInfo atIndex:0];
        recentPhotos = [photos copy];
    }
    [defaults setObject:recentPhotos forKey:RECENT_PHOTOS_KEY];
}

- (void)setInitialScale
{
    CGFloat witdhZoom = self.scrollView.bounds.size.width / \
                        self.imageView.image.size.width;
    CGFloat heightZoom = self.scrollView.bounds.size.height / \
                         self.imageView.image.size.height;
    self.scrollView.minimumZoomScale = fmin(witdhZoom, heightZoom);
    self.scrollView.maximumZoomScale = 5;
    self.scrollView.zoomScale = fmax(witdhZoom, heightZoom);
}

- (void)setInitialSize
{
    // Set zoom scale to 1.  Whithout this the scroll view size is not
    // initialized correctly.
    self.scrollView.zoomScale = 1;
    self.imageView.frame = CGRectMake(0, 0,
                                      self.imageView.image.size.width,
                                      self.imageView.image.size.height);
    self.scrollView.contentSize = self.imageView.image.size;
}

- (void)loadPhoto
{
    dispatch_queue_t downloadQueue = dispatch_queue_create(
                                            "flickr photo downloader", NULL);
    [self.downloadIndicator startAnimating];
    dispatch_async(downloadQueue, ^{
        NSDictionary *photoInfo = self.photoInfo;
        NSString *photoId = [photoInfo objectForKey:FLICKR_PHOTO_ID];
        LPCache *cache = [LPCache cacheForFacility:PHOTO_CACHE_FACILITY
                                 andMaxSizeInBytes:PHOTO_CACHE_MAX_SIZE];
        NSData *imageData = [cache dataForKey:photoId];
        if (!imageData) {
            // Photo not found in cache
            NSURL *imageUrl = [FlickrFetcher
                               urlForPhoto:photoInfo
                                    format:FlickrPhotoFormatLarge];
            imageData = [NSData dataWithContentsOfURL:imageUrl];
            [cache add:imageData forKey:photoId];
        }
        
        // do not display this photo if the user selects another one
        if (photoInfo != self.photoInfo) return;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageView.image = [UIImage imageWithData:imageData];
            [self setInitialSize];
            [self setInitialScale];
            [self.downloadIndicator stopAnimating];
        });
    });
    dispatch_release(downloadQueue);
}

- (void)setPhotoInfo:(NSDictionary *)photoInfo
{
    if (_photoInfo != photoInfo) {
        _photoInfo = photoInfo;
        [self loadPhoto];
        [self setRecentImage];
    }
}

- (void)handleSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    NSMutableArray *toolbarItems = [self.toolbar.items mutableCopy];
    if (_splitViewBarButtonItem) [toolbarItems removeObject:_splitViewBarButtonItem];
    if (splitViewBarButtonItem) [toolbarItems insertObject:splitViewBarButtonItem atIndex:0];
    self.toolbar.items = toolbarItems;
    _splitViewBarButtonItem = splitViewBarButtonItem;
}

- (void)setSplitViewBarButtonItem:(UIBarButtonItem *)splitViewBarButtonItem
{
    if (splitViewBarButtonItem != _splitViewBarButtonItem) {
        [self handleSplitViewBarButtonItem:splitViewBarButtonItem];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self handleSplitViewBarButtonItem:self.splitViewBarButtonItem];
    self.title = [[self class] photoTitleByInfo:self.photoInfo];
    self.scrollView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.imageView.image) {
        [self setInitialSize];
        [self setInitialScale];
    }
    
    [LPVacationData
     openVacation:@"My Vacation"
     usingBlock:^(UIManagedDocument* vacation) {
         BOOL photoExist = [Photo exist:self.photoInfo
                 inManagedObjectContext:vacation.managedObjectContext];
         UIBarButtonItem *visitButton = self.navigationItem.rightBarButtonItem;
         if (photoExist) {
             visitButton.title = @"Unvisit";
         } else {
             visitButton.title = @"Visit";
         }
     }];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.imageView;
}

- (void)viewDidUnload
{
    [self setImageView:nil];
    [self setScrollView:nil];
    [self setDownloadIndicator:nil];
    [self setSplitViewBarButtonItem:nil];
    [self setSplitViewBarButtonItem:nil];
    [self setToolbar:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
    } else {
        return YES;
    }
}

+ (NSString*)photoTitleByInfo:(NSDictionary*)photoInfo
{
    NSString *title = [photoInfo objectForKey:FLICKR_PHOTO_TITLE];
    if (!title || [title isEqualToString:@""]) {
        NSString *description = [photoInfo valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
        if (description &&
            ![description isEqualToString:@""] &&
            ![description isEqualToString:@"\n"]) {
            return description;
        } else {
            return @"Unknown";
        }
    }
    return title;
}
@end

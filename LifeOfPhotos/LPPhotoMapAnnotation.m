//
//  LPPhotoMapAnnotation.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPPhotoMapAnnotation.h"
#import "FlickrFetcher.h"

@implementation LPPhotoMapAnnotation
@synthesize photo = _photo;

+(LPPhotoMapAnnotation*)annotationForPhoto:(NSDictionary*)photo
{
    LPPhotoMapAnnotation *annotation = [[LPPhotoMapAnnotation alloc] init];
    annotation.photo = photo;
    return annotation;
}

#pragma mark - MKAnnotation

- (NSString *)title
{
    return [self.photo objectForKey:FLICKR_PHOTO_TITLE];
}

- (NSString*)subtitle
{
    return [self.photo valueForKeyPath:FLICKR_PHOTO_DESCRIPTION];
}

- (CLLocationCoordinate2D)coordinate
{
    CLLocationCoordinate2D coordinate;
    coordinate.latitude = [[self.photo objectForKey:FLICKR_LATITUDE] doubleValue];
    coordinate.longitude = [[self.photo objectForKey:FLICKR_LONGITUDE] doubleValue];
    return coordinate;
}
@end

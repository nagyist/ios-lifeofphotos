//
//  LPPhotoCache.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 31.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef unsigned long long filesize_t;

@interface LPCache : NSObject

/**
 *  \brief Return the cache class for the given facillity.
 *
 *  If there allready exists a cache instance for this facility, return this
 *  instance.
 */
+ (LPCache*)cacheForFacility:(NSString*)facillity
                andMaxSizeInBytes:(filesize_t)maxSize;

/**
 *  \brief Add the given NSData object to the cache.
 */
- (void)add:(NSData*)data forKey:(NSString*)key;

/**
 *  \brief Returns the NSData object for the given Key from the cache.
 *
 *  If the key can't be found it will return nil.
 */
- (NSData*)dataForKey:(NSString*)key;

@end

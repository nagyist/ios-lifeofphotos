//
//  LPVacationTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPVacationTVC.h"
#import "LPVacationData.h"
#import "LPVacationChoiceTVC.h"

@interface LPVacationTVC ()

@end

@implementation LPVacationTVC

- (char*)backgroundThreadName
{
    static char *name = "load vacations";
    return name;
}

- (NSArray*)loadTableDataInDownloadQueue
{
    return [LPVacationData allVacations];
}

- (NSString*)cellIdentifier
{
    return @"Vacation Cell";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self loadTableData];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"Show Vacation Choice"]) {
        [segue.destinationViewController setVacationName:[sender text]];
    }
}

@end

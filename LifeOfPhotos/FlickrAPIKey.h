//
//  FlickrAPIKey.h
//
//  Created for Stanford CS193p Fall 2011.
//  Copyright 2011 Stanford University. All rights reserved.
//
//  Get your own key!
//  No Flickr fetches will work without the API Key!
//  http://www.flickr.com/services/api/misc.api_keys.html
//

#define FlickrAPIKey @""

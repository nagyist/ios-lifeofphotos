//
//  LPVacationPhotosTVC.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataTableViewController.h"
#import "Tag.h"

@interface LPVacationPhotosTVC : CoreDataTableViewController

@property (nonatomic,strong) NSString *vacationName;
@property (nonatomic,strong) NSString *placeName;
@property (nonatomic,strong) Tag *tag;

@end

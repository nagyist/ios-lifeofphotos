//
//  Place+Create.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 11.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Place.h"

@interface Place (Create)

+ (Place*)create:(NSString*)name
inManagedObjectContext:(NSManagedObjectContext*)context;

@end

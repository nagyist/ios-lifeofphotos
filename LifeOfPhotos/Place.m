//
//  Place.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 15.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Place.h"
#import "Photo.h"


@implementation Place

@dynamic name;
@dynamic created;
@dynamic photos;

@end

//
//  LPVacationData.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef void (^completion_block_t)(UIManagedDocument *vacation);

@interface LPVacationData : NSObject

@property (nonatomic,strong)UIManagedDocument *vacation;

+ (void)openVacation:(NSString *)vacationName
          usingBlock:(completion_block_t)completionBlock;

+ (NSArray*)allVacations;

@end

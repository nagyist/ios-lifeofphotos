//
//  LPBaseTVC.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPBaseTVC : UITableViewController

@property (nonatomic,readonly) NSString* cellIdentifier;
@property (nonatomic,strong) NSArray* tableData;

- (void)loadTableData;

@end

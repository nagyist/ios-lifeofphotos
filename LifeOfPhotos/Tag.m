//
//  Tag.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 15.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Tag.h"
#import "Photo.h"


@implementation Tag

@dynamic name;
@dynamic photoCount;
@dynamic photos;

@end

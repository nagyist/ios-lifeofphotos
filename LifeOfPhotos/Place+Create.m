//
//  Place+Create.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 11.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Place+Create.h"

@implementation Place (Create)

+ (Place*)create:(NSString*)name
inManagedObjectContext:(NSManagedObjectContext*)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Place"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"name"
                                        ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    
    NSError *error = nil;
    NSArray *places = [context executeFetchRequest:request error:&error];
    Place *place = [places lastObject];
    if (!place) {
        place = [NSEntityDescription insertNewObjectForEntityForName:@"Place"
                                              inManagedObjectContext:context];
        place.name = name;
        place.created = [NSDate date];
    }
    return place;
}

@end

//
//  LPVacationChoiceTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPVacationChoiceTVC.h"

@interface LPVacationChoiceTVC ()

@end

@implementation LPVacationChoiceTVC
@synthesize vacationName = _vacationName;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationItem.title = self.vacationName;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [super prepareForSegue:segue sender:sender];
    if ([segue.identifier isEqualToString:@"Show Vacation Places"]) {
        [segue.destinationViewController setVacationName:self.vacationName];
    } else if ([segue.identifier isEqualToString:@"Show Vacation Tags"]) {
        [segue.destinationViewController setVacationName:self.vacationName];
    }
}

@end

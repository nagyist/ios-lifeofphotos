//
//  LPPhotosTVC.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 27.07.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPBaseTVC.h"

@interface LPPhotosTVC : LPBaseTVC

@property (nonatomic,strong) NSDictionary* place;

@end

//
//  LPVacationChoiceTVC.h
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LPVacationChoiceTVC : UITableViewController

@property (nonatomic,strong) NSString *vacationName;

@end

//
//  Tag+Create.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 09.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "Tag+Create.h"

@implementation Tag (Create)

+ (Tag*)create:(NSString*)name
inManagedObjectContext:(NSManagedObjectContext*)context
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Tag"];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    NSSortDescriptor *sortDescriptor = [NSSortDescriptor
                                        sortDescriptorWithKey:@"name"
                                        ascending:YES];
    request.sortDescriptors = @[sortDescriptor];
    
    NSError *error = nil;
    NSArray *tags = [context executeFetchRequest:request error:&error];
    Tag *tag = [tags lastObject];
    if (!tag) {
        tag = [NSEntityDescription insertNewObjectForEntityForName:@"Tag"
                                            inManagedObjectContext:context];
        tag.name = name;
        tag.photoCount = 0;
    }
    return tag;
}

+ (NSSet*)createTags:(NSArray*)names
inManagedObjectContext:(NSManagedObjectContext*)context
{
    NSMutableSet *tags = [[NSMutableSet alloc] init];
    for (NSString *name in names) {
        if ([name rangeOfString:@":"].location == NSNotFound) {
            [tags addObject:[self create:name inManagedObjectContext:context]];
        }
    }
    return tags;
}

@end

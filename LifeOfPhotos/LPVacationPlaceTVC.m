//
//  LPVacationPlaceTVC.m
//  LifeOfPhotos
//
//  Created by Maik Brendler on 07.08.12.
//  Copyright (c) 2012 Maik Brendler. All rights reserved.
//

#import "LPVacationPlaceTVC.h"
#import "LPVacationData.h"
#import "Place.h"
#import "LPVacationPhotosTVC.h"

@interface LPVacationPlaceTVC ()

@property (nonatomic,strong) UIManagedDocument *vacation;

@end

@implementation LPVacationPlaceTVC
@synthesize vacationName = _vacationName;

- (void)setupFetchedResultsController
{
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"Place"];
    request.sortDescriptors = @[
        [NSSortDescriptor sortDescriptorWithKey:@"created" ascending:YES],
        [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES],
    ];
    
    [LPVacationData
     openVacation:self.vacationName
     usingBlock:^(UIManagedDocument *vacation){
         self.fetchedResultsController = [[NSFetchedResultsController alloc]
                    initWithFetchRequest:request
                    managedObjectContext:vacation.managedObjectContext
                    sectionNameKeyPath:nil
                    cacheName:nil];
     }];
}

- (void)setVacationName:(NSString *)vacationName
{
    if (vacationName != _vacationName) {
        _vacationName = vacationName;
        [self setupFetchedResultsController];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Vacation Place Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    Place *place = [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.textLabel.text = place.name;
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"Show Vacation Table"]) {
        [segue.destinationViewController setVacationName:self.vacationName];
        [segue.destinationViewController setPlaceName:[sender text]];
    }
}

@end
